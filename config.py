# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

import os

class Config(object):

    @staticmethod
    def _get_database():
        env_name = os.environ.get('DATABASE', '')

        if env_name == 'postgres':
            uri = 'postgresql://{}:{}@{}:{}/{}'.format(
                os.environ.get('POSTGRES_USER', ''),
                os.environ.get('POSTGRES_PASSWORD', ''),
                os.environ.get('POSTGRES_HOST', 'db'),
                os.environ.get('POSTGRES_PORT', 5432),
                os.environ.get('POSTGRES_DB', '')
            )
            return uri

        basedir = os.path.abspath(os.path.dirname(__file__))
        uri = 'sqlite:///' + os.path.join(basedir, 'database.db')
        return uri

    basedir = os.path.abspath(os.path.dirname(__file__))

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    # This will create a file in <app> FOLDER
    SQLALCHEMY_DATABASE_URI = _get_database.__func__()

    # For 'in memory' database, please use:
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
            
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEFAULT_THEME = None

    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT'))
    MAIL_USE_SSL = bool(os.environ.get('MAIL_USE_SSL'))
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    # cuidado aqui. Nao tirar do padrao. Envio de email e outras funcionalidades dependem
    # da lista dessa forma. Para add mais: {'hi': 'goodbye', 'hi2': 'goodbye2'}
    ADMIN_USERS = eval(os.environ.get('ADMIN_USERS'))


class ProductionConfig(Config):
    DEBUG = False

    # Security
    SESSION_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_DURATION = 3600

    SQLALCHEMY_DATABASE_URI = Config._get_database()


class DebugConfig(Config):
    DEBUG = True


config_dict = {
    'Production': ProductionConfig,
    'Debug': DebugConfig
}
