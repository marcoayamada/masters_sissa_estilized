# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask import Flask, url_for
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from importlib import import_module
from logging import basicConfig, DEBUG, getLogger, StreamHandler
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_wtf import CSRFProtect
from flask_jsglue import JSGlue
from os import path
import re
import locale

db = SQLAlchemy()
login_manager = LoginManager()
login_manager.login_view = 'base_blueprint.login'
bootstrap = Bootstrap()
mail = Mail()
csrf = CSRFProtect()
jsglue = JSGlue()

def register_extensions(app):
    db.init_app(app)
    csrf.init_app(app)
    login_manager.init_app(app)
    bootstrap.init_app(app)
    mail.init_app(app)
    jsglue.init_app(app)

def register_blueprints(app):
    for module_name in ['base', 'admin', 'paciente', 'questionario', 'tratamento', 'home']:
        module = import_module('app.{}.routes'.format(module_name))
        app.register_blueprint(module.blueprint)

def register_errors(app):
    module = import_module('app.base.routes')
    app.register_error_handler(404, module.not_found_error)
    app.register_error_handler(403, module.access_forbidden)
    app.register_error_handler(500, module.internal_error)

def configure_database(app):

    @app.before_first_request
    def initialize_database():
        db.create_all()

    @app.teardown_request
    def shutdown_session(exception=None):
        db.session.remove()

def configure_logs(app):
    # soft logging
    try:
        basicConfig(filename='error.log', level=DEBUG)
        logger = getLogger()
        logger.addHandler(StreamHandler())
    except:
        pass

def configure_email(app):
    if not app.debug and not app.testing:
        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
            secure = None
            if app.config['MAIL_USE_SSL']:
                secure = ()

def apply_themes(app):
    """
    Add support for themes.

    If DEFAULT_THEME is set then all calls to
      url_for('static', filename='')
      will modfify the url to include the theme name

    The theme parameter can be set directly in url_for as well:
      ex. url_for('static', filename='', theme='')

    If the file cannot be found in the /static/<theme>/ location then
      the url will not be modified and the file is expected to be
      in the default /static/ location
    """
    @app.context_processor
    def override_url_for():
        return dict(url_for=_generate_url_for_theme)

    def _generate_url_for_theme(endpoint, **values):
        if endpoint.endswith('static'):
            themename = values.get('theme', None) or \
                app.config.get('DEFAULT_THEME', None)
            if themename:
                theme_file = "{}/{}".format(themename, values.get('filename', ''))
                if path.isfile(path.join(app.static_folder, theme_file)):
                    values['filename'] = theme_file
        return url_for(endpoint, **values)

def custom_jinja_filters(app):
    def regex_replace(string, pattern, repl):
        return re.sub(string=string, pattern=pattern, repl=repl)

    app.jinja_env.filters['regex_replace'] = regex_replace

def create_app(config, selenium=False):
    locale.setlocale(locale.LC_TIME, "pt_BR.UTF-8") # setando a região como pt_BR. Serve para datas.
    app = Flask(__name__, static_folder='base/static')
    custom_jinja_filters(app)
    app.config.from_object(config)
    if selenium:
        app.config['LOGIN_DISABLED'] = True
    register_extensions(app)
    register_blueprints(app)
    configure_database(app)
    configure_logs(app)
    apply_themes(app)
    configure_email(app)
    register_errors(app)
    return app
