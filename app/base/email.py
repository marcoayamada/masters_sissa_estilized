from flask import render_template, current_app
from app.email import send_email


def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email(('[Microblog] Resete sua senha de acesso'),
               sender=current_app.config['ADMIN_USERS'].get('admin'),
               recipients=[user.email],
               text_body=render_template('email/reset_password.txt', user=user, token=token),
               html_body=render_template('email/reset_password.html', user=user, token=token))

# def send_password_set_new(user):
#     token = user.get_reset_password_token()
#     send_email(('[Microblog] Cadastre sua senha de acesso'),
#                sender=current_app.config['ADMIN_USERS'][0].get('admin').get('email'),
#                recipients=[user.email],
#                text_body=render_template('email/set_password.txt', user=user, token=token),
#                html_body=render_template('email/set_password.html', user=user, token=token))
