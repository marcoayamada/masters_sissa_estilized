# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask import render_template, redirect, request, url_for, flash, current_app
from flask_login import current_user, login_required, login_user, logout_user
from app import db, login_manager
from app.base import blueprint
from app.base.forms import LoginForm
from app.admin.forms import ResetPasswordRequestForm, ResetPasswordForm
from app.base.models import User, DispensacaoMedicamento, Retorno, Paciente, EstadoSituacional
from app.base.email import send_password_reset_email
from app.base.util import verify_pass
from datetime import datetime
from sqlalchemy.sql.expression import literal

@blueprint.route('/')
def route_default():
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/index')
@login_required
def index():
    if not current_user.is_authenticated:
        return redirect(url_for('base_blueprint.login'))

    # %W considera como primeiro dia da semana a segunda-feira. Foi
    # considerado dessa forma porque a consulta nao tem o parametro para descobrir o dia.
    # Deve existir uma forma, mas para esse caso assim já funciona.
    atual_week = int(datetime.today().strftime('%W')) + 1

    primeiro_retorno = db.session.query(DispensacaoMedicamento, literal('Primeiro retorno').label("tipo"))\
        .filter(db.extract('week', DispensacaoMedicamento.data_retorno) == atual_week)\
        .order_by(DispensacaoMedicamento.data_retorno.asc())\
        .all()

    retornos_posteriores = db.session.query(Retorno, literal('Retornos posteriores').label("tipo"))\
        .filter(db.extract('week', Retorno.data_retorno) == atual_week)\
        .order_by(Retorno.data_retorno.asc())\
        .all()

    pacientes = db.session.query(Paciente.sexo,
                                 db.extract('week', Paciente.register_in),
                                 (datetime.now().date() - Paciente.data_nascimento)/365).all()

    pacientes_total_geral = len(pacientes)
    pacientes_total_semana = len([week for _, week, _ in pacientes if week == atual_week])
    paciente_porc_masculino = int(safe_divide(len([genero for genero, _, _ in pacientes if genero == 'Masculino']), pacientes_total_geral)*100)

    # melhorar isso. É possível fazer isso usando sqlalchemy.
    idade_count = {
        'ate_15': 0,
        '16_a_30': 0,
        '31_a_45': 0,
        '46_a_60': 0,
        '61_a_75': 0,
        '76_a_90': 0,
        'mais_91': 0
    }
    for _, _, idade in pacientes:
        if idade <= 15:
            idade_count['ate_15'] += 1
        if 15 < idade <= 30:
            idade_count['16_a_30'] += 1
        if 30 < idade <= 45:
            idade_count['31_a_45'] += 1
        if 45 < idade <= 60:
            idade_count['46_a_60'] += 1
        if 60 < idade <= 75:
            idade_count['61_a_75'] += 1
        if 75 < idade <= 90:
            idade_count['76_a_90'] += 1
        if idade > 91:
            idade_count['mais_91'] += 1

    atendimentos = db.session.query(db.extract('week', EstadoSituacional.register_in)).all()
    atendimentos_total = len(atendimentos)
    atendimentos_semana = len([week for week, in atendimentos if week == atual_week])

    return render_template('index.html',
                           primeiro_retorno=primeiro_retorno, retornos_posteriores=retornos_posteriores,
                           pacientes_total_semana=pacientes_total_semana, pacientes_total_geral=pacientes_total_geral,
                           paciente_porc_masculino=paciente_porc_masculino,
                           atendimentos_total=atendimentos_total, atendimentos_semana=atendimentos_semana,
                           idade_count=idade_count)

@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    if 'login' in request.form:
        username = request.form['username']
        password = request.form['password']
        user = User.query.filter_by(username=username).first()

        # Primeiro login deve ser admin. Se o username colocado estiver
        # na lista e nao existir user criado, cria-se um.
        admins = current_app.config['ADMIN_USERS']

        if username in [_username for _username, _ in admins.items()] and len(User.query.all()) == 0:
            user = User(
                username=username,
                name=username,
                email=admins.get(username),
                active=True,
                admin=True,
                password=password
            )
            db.session.add(user)
            db.session.commit()
            return render_template('login/login.html', msg='Admin criado. Faça o login novamente.', form=login_form)

        if user and verify_pass(password, user.password):
            if not user.active:
                return render_template('login/login.html', msg='Usuário desativado. Fale com a moderação.', form=login_form)
            login_user(user)
            return redirect(url_for('base_blueprint.route_default'))
        return render_template('login/login.html', msg='Usuário ou senha incorreta', form=login_form)

    if not current_user.is_authenticated:
        return render_template('login/login.html', form=login_form)
    return redirect(url_for('base_blueprint.index'))

@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    form = ResetPasswordRequestForm()
    if current_user.is_authenticated:
        return redirect(url_for('base_blueprint.index'))
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
            flash('Um email foi enviado com um link para reset da senha')
            return redirect(url_for('base_blueprint.login'))
    return render_template('reset_password/reset_password_request.html', title='Resetar a senha', form=form)

@blueprint.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('base_blueprint.index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('base_blueprint.index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        user.set_active(True)
        user.set_updated_in()
        db.session.commit()
        flash('Sua senha foi resetada com sucesso')
        return redirect(url_for('base_blueprint.index'))
    return render_template('reset_password/reset_password.html', form=form)

@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect(url_for('base_blueprint.login'))

@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('errors/page-403.html'), 403

@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('errors/page-404.html'), 404

@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('errors/page-500.html'), 500

def safe_divide(n, d):
    return n / d if d else 0
