# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, StringField, SubmitField, BooleanField
from wtforms.validators import InputRequired, Email, DataRequired, ValidationError, EqualTo
from app.base.models import User


class LoginForm(FlaskForm):
    username = StringField    ('Username', id='username_login'   , validators=[DataRequired()])
    password = PasswordField('Password', id='pwd_login'        , validators=[DataRequired()])

class SearchPessoaForm(FlaskForm):
    cpf = StringField('CPF')
    nome = StringField('Nome')
    username = StringField('Username')
    email = StringField('Email')
    submit = SubmitField('Buscar')