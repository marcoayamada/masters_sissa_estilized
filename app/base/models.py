# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String, Boolean, DateTime
import datetime
import jwt
from time import time
from app import db, login_manager
from app.base.util import hash_pass
from flask import current_app
from sqlalchemy.sql.functions import ReturnTypeFromArgs
from unidecode import unidecode


class User(db.Model, UserMixin):

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    email = Column(String, unique=True)
    name = Column(String)
    password = Column(Binary)
    active = db.Column(Boolean, unique=False, default=True)
    admin = db.Column(Boolean, unique=False, default=False)
    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)
    updated_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    estado_situacional = db.relationship("EstadoSituacional", back_populates="user")
    dispensacao_medicamento = db.relationship("DispensacaoMedicamento", back_populates="user")
    avaliacao_farmacoterapia = db.relationship("AvaliacaoFarmacoterapia", back_populates="user")
    retorno = db.relationship("Retorno", back_populates="user")
    paciente = db.relationship("Paciente", back_populates="user")

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            if property == 'password':
                value = hash_pass( value ) # we need bytes here (not plain str)
                
            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)

    def set_password(self, password):
        self.password = hash_pass(password)

    def set_active(self, status):
        self.active = status

    def set_updated_in(self):
        self.updated_in = datetime.datetime.utcnow()

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

class Paciente(db.Model):

    __tablename__ = 'paciente'

    """
    Variaveis no plural correspondem a campos que poderiam muito bem
    ser 1:N. Verificar se não é melhor quebrar em mais tabelas.
    """
    id = db.Column(db.Integer, primary_key=True)
    cpf = db.Column(db.String(11), unique=True, index=True)
    nome = db.Column(db.String(200))
    data_nascimento = db.Column(db.Date)
    sexo = db.Column(db.String(20))
    #contato
    telefone = db.Column(db.String(20))
    email = db.Column(db.String(500))
    #endereço
    cep = db.Column(db.String(20))
    endereco = db.Column(db.String(500))
    complemento = db.Column(db.String(500))
    referencia = db.Column(db.String(500))
    #log
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)
    updated_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    user = db.relationship("User", back_populates="paciente")
    estados_situacionais = db.relationship('EstadoSituacional', backref='paciente', lazy='dynamic')
    dispensacoes_medicamentos = db.relationship('DispensacaoMedicamento', backref='paciente', lazy='dynamic')
    avaliacoes_farmacoterapias = db.relationship('AvaliacaoFarmacoterapia', backref='paciente', lazy='dynamic')
    retornos = db.relationship('Retorno', backref='paciente', lazy='dynamic')

    def __repr__(self):
        return '<Paciente {}>'.format(self.nome)

class EstadoSituacional(db.Model):

    __tablename__ = 'estado_situacional'

    id = db.Column(db.Integer, primary_key=True)
    patologia = db.Column(db.String(1000))
    comorbidades = db.Column(db.String(1000))
    alergia_alimentos = db.Column(db.String(1000))
    alergia_medicamentos = db.Column(db.String(1000))
    estilos_vida = db.Column(db.String(1000))
    preocupacoes = db.Column(db.String(500))
    paciente_id = db.Column(db.Integer, db.ForeignKey('paciente.id'))
    #log
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    user = db.relationship("User", back_populates="estado_situacional")
    farmacoterapia = db.relationship('Farmacoterapia', backref='EstadoSituacional', cascade="all,delete")
    dispensacao_medicamento = db.relationship("DispensacaoMedicamento", uselist=False, back_populates="estado_situacional")

    def __repr__(self):
        return '<EstadoSituacional {}>'.format(self.id)

class Farmacoterapia(db.Model):

    __tablename__ = 'farmacoterapia'

    id = db.Column(db.Integer, primary_key=True)
    nome_remedio = db.Column(db.String(500))
    concentracao = db.Column(db.String(500))
    forma_farmaceutica = db.Column(db.String(500))
    finalidade = db.Column(db.String(500))
    como_prescrito = db.Column(db.String(500))
    como_toma = db.Column(db.String(500))
    quem_receitou = db.Column(db.String(500))
    produz_efeito = db.Column(Boolean, default=False)
    dificuldade_adquirir = db.Column(Boolean, default=False)
    dificuldade_administrar = db.Column(Boolean, default=False)
    reacao_adversa = db.Column(Boolean, default=False)
    observacoes = db.Column(db.String(500))
    estado_situacional_id = db.Column(db.Integer, db.ForeignKey('estado_situacional.id'))
    #log
    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    estado_situacional = db.relationship('EstadoSituacional', backref='Farmacoterapia')
    avaliacao_subitens = db.relationship('AvaliacaoFarmacoterapiaEfetividadeSeguranca', uselist=False, back_populates='farmacoterapia')

    def __repr__(self):
        return '<Farmacoterapia {}>'.format(self.id)

class InformacaoTratamento(db.Model):

    __tablename__ = 'informacao_tratamento'

    id = db.Column(db.Integer, primary_key=True)
    protocolo = db.Column(db.String(50))
    medicamento = db.Column(db.String(50))
    concentracao = db.Column(db.String(50))
    forma_farmaceutica = db.Column(db.String(50))
    dose_usual = db.Column(db.String(5000))
    associacoes_literatura = db.Column(db.String(5000))
    indicacoes_mecanismo = db.Column(db.String(5000))
    forma_administracao = db.Column(db.String(5000))
    principais_rams = db.Column(db.String(5000))
    interacoes_graves = db.Column(db.String(5000))
    relacao_medicamento_doenca = db.Column(db.String(5000))
    interacoes_medicamento_alimento = db.Column(db.String(5000))
    ajuste_dose = db.Column(db.String(5000))
    cuidados_especiais = db.Column(db.String(5000))
    risco_gravidez = db.Column(db.String(5000))
    armazenamento_transporte = db.Column(db.String(5000))
    referencia = db.Column(db.String(5000))

    def __repr__(self):
        return '<InformacaoTratamento {}>'.format(self.id)

class DispensacaoMedicamento(db.Model):

    __tablename__ = 'dispensacao_medicamento'

    id = db.Column(db.Integer, primary_key=True)
    data_retorno = db.Column(DateTime)
    confirmacao_academico = db.Column(Boolean, default=False)
    id_medicamentos = db.Column(db.String(200))  # economia de tabelas
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    paciente_id = db.Column(db.Integer, db.ForeignKey('paciente.id'))
    estado_situacional_id = db.Column(Integer, db.ForeignKey('estado_situacional.id'))
    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    user = db.relationship("User", back_populates="dispensacao_medicamento")
    estado_situacional = db.relationship("EstadoSituacional", back_populates="dispensacao_medicamento")
    avaliacao_farmacoterapia = db.relationship("AvaliacaoFarmacoterapia", uselist=False, back_populates="dispensacao_medicamento")

    def __repr__(self):
        return '<DispensacaoMedicamento {}>'.format(self.id)

class AvaliacaoFarmacoterapia(db.Model):
    __tablename__ = 'avaliacao_farmacoterapia'

    id = db.Column(db.Integer, primary_key=True)
    necessidade_p1 = db.Column(db.String(13))
    necessidade_p2 = db.Column(db.String(13))
    adesao_p1 = db.Column(db.String(13))
    adesao_p2 = db.Column(db.String(13))
    necessidade_obs = db.Column(db.String(2000))
    adesao_obs = db.Column(db.String(2000))

    #outras observacoes
    outras_obs_p1 = db.Column(db.String(13))
    outras_obs_p2 = db.Column(db.String(13))
    outras_obs_p3 = db.Column(db.String(13))
    outras_obs_p4 = db.Column(db.String(13))
    outras_obs_p5 = db.Column(db.String(13))
    outras_obs_p6 = db.Column(db.String(13))
    outras_obs_p7 = db.Column(db.String(13))
    outras_obs_p8 = db.Column(db.String(13))
    outras_perguntas_obs = db.Column(db.String(2000))
    parecer_farmaceutico = db.Column(db.String(10000))

    paciente_id = db.Column(db.Integer, db.ForeignKey('paciente.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    dispensacao_medicamento_id = db.Column(Integer, db.ForeignKey('dispensacao_medicamento.id'))

    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)
    updated_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    dispensacao_medicamento = db.relationship("DispensacaoMedicamento", back_populates="avaliacao_farmacoterapia")
    user = db.relationship("User", back_populates="avaliacao_farmacoterapia")
    efetividades_segurancas = db.relationship("AvaliacaoFarmacoterapiaEfetividadeSeguranca", back_populates="avaliacao_farmacoterapia", cascade="all,delete")
    retornos = db.relationship("Retorno", back_populates="avaliacao_farmacoterapia")

class AvaliacaoFarmacoterapiaEfetividadeSeguranca(db.Model):
    """
    1:N que guarda campos de efetividade e segurança da tabela AvaliacaoFarmacoterapia
    """
    __tablename__ = 'avaliacao_farmacoterapia_subitens'

    id = db.Column(db.Integer, primary_key=True)
    efetividade_p1 = db.Column(db.String(13))
    efetividade_p2 = db.Column(db.String(13))
    seguranca_p1 = db.Column(db.String(13))
    seguranca_p2 = db.Column(db.String(13))
    efetividade_obs = db.Column(db.String(2000))
    seguranca_obs = db.Column(db.String(2000))
    farmacoterapia_id = Column(db.Integer, db.ForeignKey('farmacoterapia.id'))
    avaliacao_farmacoterapia_id = Column(db.Integer, db.ForeignKey('avaliacao_farmacoterapia.id'))

    avaliacao_farmacoterapia = db.relationship("AvaliacaoFarmacoterapia", back_populates="efetividades_segurancas")
    farmacoterapia = db.relationship("Farmacoterapia", back_populates="avaliacao_subitens")

    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)

class Retorno(db.Model):

    __tablename__ = 'retorno'

    id = db.Column(db.Integer, primary_key=True)
    observacoes = db.Column(db.String(2000))
    data_retorno = db.Column(DateTime)
    compareceu = db.Column(Boolean, default=False)
    avaliacao_id = db.Column(Integer, db.ForeignKey('avaliacao_farmacoterapia.id'))
    paciente_id = db.Column(db.Integer, db.ForeignKey('paciente.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    register_in = db.Column(DateTime, default=datetime.datetime.utcnow)

    user = db.relationship("User", back_populates="retorno")
    avaliacao_farmacoterapia = db.relationship("AvaliacaoFarmacoterapia", back_populates="retornos")

@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()

@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None

class Unaccent(ReturnTypeFromArgs):
    pass

class Search:

    @staticmethod
    def removeAccent(string):
        return unidecode(string)
