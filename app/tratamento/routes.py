# -*- encoding: utf-8 -*-

from flask import render_template, request, redirect, url_for
from flask_login import login_required
from app.tratamento import blueprint
from app import db
from app.base.models import InformacaoTratamento, Unaccent, Search
from app.tratamento.forms import SearchInformacaoTratamentoForm


@blueprint.route('/listar', methods=['GET', 'POST'])
@login_required
def listar_tratamentos():
    pagina = request.args.get('pagina', default=1, type=int)
    protocolo = request.args.get('protocolo', None, type=str)
    medicamento = request.args.get('medicamento', None, type=str)

    search_form = SearchInformacaoTratamentoForm()
    if search_form.validate_on_submit():
        return redirect(url_for('tratamento_blueprint.listar_tratamentos',
                                pagina=1,
                                protocolo=search_form.protocolo.data,
                                medicamento=search_form.medicamento.data))
    elif request.method == 'GET':
        search_form.protocolo.data = protocolo
        search_form.medicamento.data = medicamento

    qtd_pagina = 8
    query_string = []
    if protocolo:
        query_string.append(InformacaoTratamento.protocolo == protocolo)
    if medicamento:
        str_unnacented = Search.removeAccent(medicamento)
        query_string.append(Unaccent(InformacaoTratamento.medicamento).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(InformacaoTratamento).filter(db.and_(*query_string))

    tratamentos = query.paginate(pagina, qtd_pagina, error_out=False)

    return render_template('tratamentos.html',  title='Tratamentos', tratamentos=tratamentos,
                           search_form=search_form)

@blueprint.route('/detalhes/<int:id_tratamento>', methods=['GET', 'POST'])
@login_required
def detalhar_tratamento(id_tratamento):
    tratamento = InformacaoTratamento.query.filter_by(id=id_tratamento).first()
    return render_template('tratamento_detalhe.html', title='Tratamento detalhe', tratamento=tratamento)
