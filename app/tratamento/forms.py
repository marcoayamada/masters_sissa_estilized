# -*- encoding: utf-8 -*-

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField

class SearchInformacaoTratamentoForm(FlaskForm):

    choices = [('', 'Selecione'), ('Doença de Crohn', 'Doença de Crohn'), ('Colite Ulcerativa', 'Colite Ulcerativa')]

    protocolo = SelectField('Protocolo', choices=choices)
    medicamento = StringField('Medicamento')

    submit = SubmitField('Buscar')
