from flask import Blueprint

blueprint = Blueprint('paciente_blueprint', __name__, template_folder='templates', url_prefix='/paciente')
