from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, DateField, SelectField
from wtforms.validators import ValidationError, DataRequired, Length, NoneOf
from app.base.models import Paciente


class PacienteForm(FlaskForm):
    cpf = StringField('CPF', validators=[DataRequired(), Length(min=11, max=11, message='Digite o CPF apenas com números e 11 caracteres')], render_kw={'autocomplete': 'off'})
    nome = StringField('Nome', validators=[DataRequired(), Length(min=-1, max=200, message='Digite um nome menor.')], render_kw={'autocomplete': 'off'})
    data_nascimento = DateField('Data de nascimento', format='%d/%m/%Y', validators=[DataRequired('Preencha com uma data.')], render_kw={'autocomplete': 'off'})
    sexo = SelectField('Sexo', choices=[('selecione', 'Selecione...'), ('Feminino', 'Feminino'), ('Masculino', 'Masculino')],
                       validators=[NoneOf('selecione', message="Escolha uma opção válida")], render_kw={'autocomplete': 'off'})
    telefone = StringField('Telefone', validators=[Length(min=-1, max=20, message='Digite um telefone menor.')], render_kw={'autocomplete': 'off'})
    email = StringField('Email', validators=[Length(min=-1, max=500, message='Digite um email menor.')], render_kw={'autocomplete': 'off'})
    cep = StringField('CEP', validators=[Length(min=-1, max=8, message='Digite apenas os números.')], render_kw={'autocomplete': 'off'})
    endereco = StringField('Endereço', validators=[Length(min=-1, max=500, message='Digite um endereço menor.')], render_kw={'autocomplete': 'off'})
    complemento = StringField('Complemento', validators=[Length(min=-1, max=500, message='Digite um complemento menor.')], render_kw={'autocomplete': 'off'})
    referencia = StringField('Ponto de referência', validators=[Length(min=-1, max=500, message='Digite uma referência menor.')], render_kw={'autocomplete': 'off'})
    submit = SubmitField('Salvar')

    def validate_cpf(self, cpf):
        paciente = Paciente.query.filter_by(cpf=cpf.data).first()
        if paciente is not None:
            raise ValidationError('Esse CPF já existe na base. Cadastre outro.')

class EditPacienteForm(PacienteForm):
    cpf = StringField('CPF', render_kw={'readonly': True})

    def validate_cpf(self, cpf):
        pass

    def __init__(self, original_cpf, *args, **kwargs):
        super(EditPacienteForm, self).__init__(*args, **kwargs)
        self.original_cpf = original_cpf
