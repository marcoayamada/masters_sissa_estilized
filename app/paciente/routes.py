# -*- encoding: utf-8 -*-

from flask import render_template, redirect, request, url_for, flash
from flask_login import current_user, login_required
import datetime
from app import db
from app.paciente import blueprint
from app.paciente.forms import PacienteForm, EditPacienteForm
from app.base.forms import SearchPessoaForm
from app.base.models import Paciente, Unaccent, Search


@blueprint.route('/listar', methods=['GET', 'POST'])
@login_required
def pacientes():
    pagina = request.args.get('pagina', default=1, type=int)
    cpf = request.args.get('cpf', None, type=str)
    nome = request.args.get('nome', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return redirect(url_for('paciente_blueprint.pacientes', pagina=1, cpf=search_form.cpf.data, nome=search_form.nome.data))
    elif request.method == 'GET':
        search_form.cpf.data = cpf
        search_form.nome.data = nome

    qtd_pagina = 10
    query_string = []
    if cpf:
        query_string.append(Paciente.cpf == cpf)
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(Paciente.nome).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(Paciente).filter(db.and_(*query_string))

    pacientes = query.order_by("nome").paginate(pagina, qtd_pagina, error_out=False)

    return render_template('pacientes.html', title='Pacientes', pacientes=pacientes, search_form=search_form)

@blueprint.route('/adicionar', methods=['GET', 'POST'])
@login_required
def adicionar_paciente():
    if not current_user.is_authenticated:
        return redirect(url_for('base_blueprint.login'))

    form = PacienteForm()
    if form.validate_on_submit():
        paciente = Paciente(
             cpf=form.cpf.data,
             nome=form.nome.data,
             data_nascimento=form.data_nascimento.data,
             sexo=form.sexo.data,
             telefone=form.telefone.data,
             email=form.email.data,
             cep=form.cep.data,
             endereco=form.endereco.data,
             complemento=form.complemento.data,
             referencia=form.referencia.data,
             user=current_user
        )
        db.session.add(paciente)
        db.session.commit()
        flash('Usuário adicionado com sucesso')
        return redirect(url_for('paciente_blueprint.pacientes', pagina=1))
    return render_template('paciente.html', methods=['GET', 'POST'], form=form, titulo_card="Adicionar paciente")

@blueprint.route('/editar/<id>', methods=['GET', 'POST'])
@login_required
def edit_paciente(id):
    paciente = Paciente.query.filter_by(id=id).first()
    form = EditPacienteForm(paciente.cpf)
    if form.validate_on_submit():
        paciente.nome = form.nome.data
        paciente.data_nascimento = form.data_nascimento.data
        paciente.sexo = form.sexo.data
        paciente.telefone = form.telefone.data
        paciente.email = form.email.data
        paciente.cep = form.cep.data
        paciente.endereco = form.endereco.data
        paciente.complemento = form.complemento.data
        paciente.referencia = form.referencia.data
        paciente.updated_in = datetime.datetime.utcnow()
        db.session.commit()
        flash('Paciente editado com sucesso')
        return redirect(url_for('paciente_blueprint.pacientes', pagina=1))
    elif request.method == 'GET':
        form.cpf.data = paciente.cpf
        form.nome.data = paciente.nome
        form.data_nascimento.data = paciente.data_nascimento
        form.sexo.data = paciente.sexo
        form.telefone.data = paciente.telefone
        form.email.data = paciente.email
        form.cep.data = paciente.cep
        form.endereco.data = paciente.endereco
        form.complemento.data = paciente.complemento
        form.referencia.data = paciente.referencia
    return render_template('paciente.html', methods=['GET', 'POST'], form=form, titulo_card="Editar paciente")
