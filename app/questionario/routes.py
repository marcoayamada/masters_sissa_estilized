# -*- encoding: utf-8 -*-

from flask import render_template, redirect, request, url_for, flash, jsonify
from flask_login import current_user, login_required
from app.questionario import blueprint
import datetime
from app import db
from app.questionario.forms import monta_form_personalizado, DispensacaoMedicamentoForm, AvaliacaoProblemasFarmacoterapia, AvaliacaoFarmacoterapiaEfetividadeSegurancaForm, RetornoForm
from app.base.forms import SearchPessoaForm
from app.base.models import Paciente, EstadoSituacional, Farmacoterapia, DispensacaoMedicamento, InformacaoTratamento, AvaliacaoFarmacoterapia, AvaliacaoFarmacoterapiaEfetividadeSeguranca, Retorno, Search, Unaccent
from app.questionario.utils import junta_respostas

@blueprint.route('/detalhe_atendimento/<int:id_paciente>', methods=['GET', 'POST'])
@login_required
def detalhe_atendimento(id_paciente):

    avaliacao_form = AvaliacaoProblemasFarmacoterapia()
    avaliacao_sub_form = AvaliacaoFarmacoterapiaEfetividadeSegurancaForm()

    hoje = datetime.date.today()

    estado_situacional = EstadoSituacional.query.filter_by(paciente_id=id_paciente).first()

    dispensacao_medicamento = getattr(estado_situacional, "dispensacao_medicamento", None)

    tratamentos = None
    if dispensacao_medicamento:
        dispensacao_tratamentos = dispensacao_medicamento.id_medicamentos.split(',')
        if len(dispensacao_tratamentos) and dispensacao_tratamentos[0] == '':
            dispensacao_tratamentos = []

        tratamentos = InformacaoTratamento.query.filter(InformacaoTratamento.id.in_(dispensacao_tratamentos)).all()

    avaliacao_medicamento = getattr(dispensacao_medicamento, "avaliacao_farmacoterapia", None)

    retornos = getattr(avaliacao_medicamento, "retornos", None)

    return render_template('detalhe_atendimento.html',
                           title='Detalhe do atendimento',
                           hoje=hoje,
                           estado_situacional=estado_situacional,
                           dispensacao_medicamento=dispensacao_medicamento, tratamentos=tratamentos,
                           avaliacao_medicamento=avaliacao_medicamento, avaliacao_form=avaliacao_form, avaliacao_sub_form=avaliacao_sub_form,
                           retornos=retornos)

@blueprint.route('/estado_situacional/seleciona-pessoa', methods=['GET', 'POST'])
@login_required
def selecionar_pessoa_estado_situacional():
    cpf = request.args.get('cpf', None, type=str)
    nome = request.args.get('nome', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return jsonify(status='ok')
    elif request.method == 'GET':
        search_form.cpf.data = cpf
        search_form.nome.data = nome

    limit = 5
    query_string = []
    if cpf:
        query_string.append(Paciente.cpf == cpf)
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(Paciente.nome).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(Paciente).filter(db.and_(*query_string))

    pacientes = query.order_by(Paciente.register_in.desc()).limit(limit).all()
    return render_template('estado_situacional_seleciona_paciente.html', title='Estados situacionais',
                           pacientes=pacientes, search_form=search_form)

@blueprint.route('/estado_situacional/cadastrar', methods=['GET', 'POST'])
@login_required
def cadastrar_estado_situacional():
    paciente_cpf = request.args.get('paciente', None, type=str)

    paciente = db.session.query(Paciente).filter(Paciente.cpf == paciente_cpf).first()

    if paciente is None:
        flash('[Estado Situacional] Paciente não está cadastrado.')
        return redirect(url_for('questionario_blueprint.listar_estado_situacional', pagina=1))

    comorbidades = [('0', 'HAS'), ('1', 'Dislipidemias'), ('2', 'Diabetes'), ('3', 'Hipotireoidismo'), ('4', 'Asma'),
                    ('5', 'Obesidade'), ('6', 'Mal de Alzheimer'), ('7', 'Doença de Parkinson')]
    patologias = [('1', 'Doença de Crohn'), ('2', 'Colite Ulcerativa')]
    estilos_vida = [('0', 'Tabaco'), ('1', 'Álcool'), ('2', 'Dieta balanceada'), ('3', 'Exercícios físicos regulares')]
    alergias_medicamentos = [('0', 'AINES'), ('1', 'Penicilinas')]
    alergias_alimentos = [('0', 'Lactose'), ('1', 'Glúten')]

    form = monta_form_personalizado(
        _comorbidades=comorbidades,
        _patologias=patologias,
        _estilos_vida=estilos_vida,
        _alergias_alimentos=alergias_alimentos,
        _alergias_medicamentos=alergias_medicamentos)

    if form.validate_on_submit():

        farmacoterapias=[]
        for historico in form.historicoRemediosUsados.data:
            farmacoterapia = Farmacoterapia(
                nome_remedio=historico.get('nomeRemedio'),
                concentracao=historico.get('concentracao'),
                forma_farmaceutica=historico.get('formaFarmaceutica'),
                finalidade=historico.get('finalidade'),
                como_prescrito=historico.get('comoFoiPrescrito'),
                como_toma=historico.get('comoToma'),
                quem_receitou=historico.get('quemReceitou'),
                produz_efeito=historico.get('produzEfeitoEsperado'),
                dificuldade_adquirir=historico.get('temDificuldadeAdquirir'),
                dificuldade_administrar=historico.get('temDificuldadeAdministrar'),
                reacao_adversa=historico.get('senteReacaoAdversa'),
                observacoes=historico.get('observacoes')
            )
            farmacoterapias.append(farmacoterapia)

        estado_situacional = EstadoSituacional(
            patologia=dict(patologias).get(form.patologias.data),
            comorbidades=junta_respostas(comorbidades, form.comorbidades.data, form.outrasComorbidades.data),
            alergia_alimentos=junta_respostas(alergias_alimentos, form.alergias_alimentos.data, form.outrasAlergiasAlimentos.data),
            alergia_medicamentos=junta_respostas(alergias_medicamentos, form.alergias_medicamentos.data, form.outrasAlergiasMedicamentos.data),
            estilos_vida=junta_respostas(estilos_vida, form.estilos_vida.data, form.outrasEstiloVida.data),
            preocupacoes=form.preocupacao.data,
            paciente=paciente,
            farmacoterapia=farmacoterapias,
            user_id=current_user.id
        )
        db.session.add(estado_situacional)
        db.session.commit()
        flash('[Estado Situacional] Formulário respondido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_estado_situacional', pagina=1))
    return render_template('estado_situacional.html', methods=['GET', 'POST'], form=form, paciente=paciente, hoje=datetime.date.today())

@blueprint.route('/estado_situacional/apagar/<id>', methods=['GET'])
@login_required
def apagar_estado_situacional(id):
    estado_situacional = EstadoSituacional.query.filter_by(id=id).first()

    if estado_situacional is None:
        flash('[Estado situacional] Cadastro não encontrado')
        return redirect(url_for('questionario_blueprint.listar_estado_situacional', pagina=1))

    if estado_situacional.dispensacao_medicamento is None:
        db.session.delete(estado_situacional)
        db.session.commit()
        flash('[Estado situacional] Cadastro removido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_estado_situacional', pagina=1))

    flash('[Estado situacional] Dispensação de medicamentos já foi cadastrada')
    return redirect(url_for('questionario_blueprint.listar_estado_situacional', pagina=1))

@blueprint.route('/estado_situacional/listar', methods=['GET', 'POST'])
@login_required
def listar_estado_situacional():
    pagina = request.args.get('pagina', default=1, type=int)
    cpf = request.args.get('cpf', None, type=str)
    nome = request.args.get('nome', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return redirect(
            url_for('questionario_blueprint.listar_estado_situacional', pagina=1, cpf=search_form.cpf.data, nome=search_form.nome.data))
    elif request.method == 'GET':
        search_form.cpf.data = cpf
        search_form.nome.data = nome

    qtd_pagina = 10
    query_string = []
    if cpf:
        query_string.append(Paciente.cpf == cpf)
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(Paciente.nome).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(EstadoSituacional).filter(db.and_(*query_string))

    estados_situacionais = query.order_by(EstadoSituacional.id.desc()).paginate(pagina, qtd_pagina, error_out=False)

    return render_template('estados_situacionais.html', title='Estados situacionais',
                           estados_situacionais=estados_situacionais, search_form=search_form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/cadastrar/<int:id_estado_situacional>', methods=['GET', 'POST'])
@login_required
def cadastrar_dispensacao_medicamento(id_estado_situacional):
    form = DispensacaoMedicamentoForm()
    # verificando se ja existe uma dispensacao
    existe_dispensacao_medicamento = len(DispensacaoMedicamento.query.filter_by(estado_situacional_id=id_estado_situacional).all())

    if existe_dispensacao_medicamento > 0:
        flash('Dispensação já foi realizada.')
        return redirect(url_for('questionario_blueprint.listar_estado_situacional'))

    estado_situacional = EstadoSituacional.query.filter_by(id=id_estado_situacional).first()
    medicamentos = InformacaoTratamento.query.filter_by(protocolo=estado_situacional.patologia).order_by("medicamento", "concentracao", "forma_farmaceutica").all()

    if form.validate_on_submit():
        dispensacao = DispensacaoMedicamento(
            data_retorno=form.data_retorno.data,
            confirmacao_academico=form.check_academico.data,
            id_medicamentos=form.remedios_dispensados.data,
            estado_situacional=estado_situacional,
            paciente_id=estado_situacional.paciente_id,
            user_id=current_user.id
        )
        db.session.add(dispensacao)
        db.session.commit()
        flash('[Dispensação de medicamentos] Formulário respondido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_dispensacao_medicamento', pagina=1))

    return render_template('dispensacao_medicamento.html', title='Dispensação medicamento',
                           estado_situacional=estado_situacional, medicamentos=medicamentos,
                           hoje=datetime.date.today(), form=form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/apagar/<id>', methods=['GET'])
@login_required
def apagar_dispensacao_medicamento(id):
    dispensacao_medicamento = DispensacaoMedicamento.query.filter_by(id=id).first()

    if dispensacao_medicamento is None:
        flash('[Dispensação medicamento] Cadastro não encontrado')
        return redirect(url_for('questionario_blueprint.listar_dispensacao_medicamento', pagina=1))

    if dispensacao_medicamento.avaliacao_farmacoterapia is None:
        db.session.delete(dispensacao_medicamento)
        db.session.commit()
        flash('[Dispensação medicamento] Cadastro removido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_dispensacao_medicamento', pagina=1))

    flash('[Dispensação medicamento] Dispensação de medicamentos já foi cadastrada')
    return redirect(url_for('questionario_blueprint.listar_dispensacao_medicamento', pagina=1))

@blueprint.route('/estado_situacional/dispensacao_medicamentos/listar', methods=['GET', 'POST'])
@login_required
def listar_dispensacao_medicamento():
    pagina = request.args.get('pagina', default=1, type=int)
    cpf = request.args.get('cpf', None, type=str)
    nome = request.args.get('nome', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return redirect(
            url_for('questionario_blueprint.listar_dispensacao_medicamento', pagina=1, cpf=search_form.cpf.data, nome=search_form.nome.data))
    elif request.method == 'GET':
        search_form.cpf.data = cpf
        search_form.nome.data = nome

    qtd_pagina = 10
    query_string = []
    if cpf:
        query_string.append(Paciente.cpf == cpf)
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(Paciente.nome).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(DispensacaoMedicamento).filter(db.and_(*query_string))

    dispensacoes_medicamentosas = query.order_by(DispensacaoMedicamento.id.desc()).paginate(pagina, qtd_pagina, error_out=False)

    return render_template('dispensacoes_medicamentos.html', title='Dispensações medicamentosas',
                           search_form=search_form, dispensacoes_medicamentosas=dispensacoes_medicamentosas)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/<int:id_dispensacao_medicamento>/avaliacao_farmacoterapia/cadastrar', methods=['GET', 'POST'])
@login_required
def cadastrar_avaliacao_farmacoterapia(id_dispensacao_medicamento):
    dispensacao_medicamento = DispensacaoMedicamento.query.filter_by(id=id_dispensacao_medicamento).first()
    estado_situacional = dispensacao_medicamento.estado_situacional

    # pegando lista de id's e nome das farmacoterapias
    farmacoterapias_id = [farmacoterapia.id for farmacoterapia in estado_situacional.farmacoterapia]
    farmacoterapias_nome_remedio = [farmacoterapia.nome_remedio for farmacoterapia in estado_situacional.farmacoterapia]

    # o subform pede que se passe uma lista de items dinamicos. Nesse caso é a quantidade de farmacoterapias.
    form = AvaliacaoProblemasFarmacoterapia(efetividades_segurancas=farmacoterapias_id)

    # preenchendo o hiddenfield com o id da farmacoterapia. isso serve para popular os campos que eu preciso.
    # para cada subitem no form, vou pegando uma posição das 2 listas de farmacoterapia.
    for index, hid in enumerate(form.efetividades_segurancas):
        hid.farmacoterapia_id.data = farmacoterapias_id[index]
        hid.farmacoterapia_nome_remedio.data = farmacoterapias_nome_remedio[index]

    if form.validate_on_submit():
        efetividades_segurancas = []
        for subitem in form.efetividades_segurancas:
            efetividade_seguranca_item = AvaliacaoFarmacoterapiaEfetividadeSeguranca(
                efetividade_p1=subitem.efetividade_p1.data,
                efetividade_p2=subitem.efetividade_p2.data,
                seguranca_p1=subitem.seguranca_p1.data,
                seguranca_p2=subitem.seguranca_p2.data,
                efetividade_obs=subitem.efetividade_obs.data,
                seguranca_obs=subitem.seguranca_obs.data,
                farmacoterapia_id=subitem.farmacoterapia_id.data
            )
            efetividades_segurancas.append(efetividade_seguranca_item)

        avaliacao_farmacoterapia = AvaliacaoFarmacoterapia(
            necessidade_p1=form.necessidade_p1.data,
            necessidade_p2=form.necessidade_p2.data,
            adesao_p1=form.adesao_p1.data,
            adesao_p2=form.adesao_p2.data,
            necessidade_obs=form.necessidade_obs.data,
            adesao_obs=form.adesao_obs.data,
            dispensacao_medicamento=dispensacao_medicamento,
            paciente_id=dispensacao_medicamento.paciente_id,
            efetividades_segurancas=efetividades_segurancas,
            outras_obs_p1=form.outras_obs_p1.data,
            outras_obs_p2=form.outras_obs_p2.data,
            outras_obs_p3=form.outras_obs_p3.data,
            outras_obs_p4=form.outras_obs_p4.data,
            outras_obs_p5=form.outras_obs_p5.data,
            outras_obs_p6=form.outras_obs_p6.data,
            outras_obs_p7=form.outras_obs_p7.data,
            outras_obs_p8=form.outras_obs_p8.data,
            parecer_farmaceutico=form.parecer_farmaceutico.data,
            outras_perguntas_obs=form.outras_perguntas_obs.data,
            user=current_user
        )
        db.session.add(avaliacao_farmacoterapia)
        db.session.commit()
        flash('[Avaliação da farmacoterapia] Formulário respondido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1))
    return render_template('avaliacao_farmacoterapia.html', title='Avaliação da farmacoterapia',
                           methods=['GET', 'POST'], form=form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/avaliacao_farmacoterapia/<int:id_avaliacao_farmacoterapia>/editar', methods=['GET', 'POST'])
@login_required
def editar_avaliacao_farmacoterapia(id_avaliacao_farmacoterapia):
    avaliacao = AvaliacaoFarmacoterapia.query.filter_by(id=id_avaliacao_farmacoterapia).first()

    form = AvaliacaoProblemasFarmacoterapia(efetividades_segurancas=[item.id for item in avaliacao.efetividades_segurancas])

    if form.validate_on_submit():
        # prenchendo o subform de efetividade e segurança
        for sub_form, sub_data in zip(form.efetividades_segurancas, avaliacao.efetividades_segurancas):
            sub_data.efetividade_p1 = sub_form.efetividade_p1.data
            sub_data.efetividade_p2 = sub_form.efetividade_p2.data
            sub_data.seguranca_p1 = sub_form.seguranca_p1.data
            sub_data.seguranca_p2 = sub_form.seguranca_p2.data
            sub_data.efetividade_obs = sub_form.efetividade_obs.data
            sub_data.seguranca_obs = sub_form.seguranca_obs.data

        avaliacao.necessidade_p1 = form.necessidade_p1.data
        avaliacao.necessidade_p2 = form.necessidade_p2.data
        avaliacao.adesao_p1 = form.adesao_p1.data
        avaliacao.adesao_p2 = form.adesao_p2.data
        avaliacao.necessidade_obs = form.necessidade_obs.data
        avaliacao.adesao_obs = form.adesao_obs.data
        avaliacao.outras_obs_p1 = form.outras_obs_p1.data
        avaliacao.outras_obs_p2 = form.outras_obs_p2.data
        avaliacao.outras_obs_p3 = form.outras_obs_p3.data
        avaliacao.outras_obs_p4 = form.outras_obs_p4.data
        avaliacao.outras_obs_p5 = form.outras_obs_p5.data
        avaliacao.outras_obs_p6 = form.outras_obs_p6.data
        avaliacao.outras_obs_p7 = form.outras_obs_p7.data
        avaliacao.outras_obs_p8 = form.outras_obs_p8.data
        avaliacao.outras_perguntas_obs = form.outras_perguntas_obs.data
        avaliacao.parecer_farmaceutico = form.parecer_farmaceutico.data
        avaliacao.updated_in = datetime.datetime.utcnow()

        db.session.commit()
        flash('[Avaliação da farmacoterapia] Formulário editado com sucesso')
        return redirect(url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1))
    elif request.method == 'GET':

        # prenchendo o subform de efetividade e segurança
        for sub_form, data in zip(form.efetividades_segurancas, avaliacao.efetividades_segurancas):
            sub_form.efetividade_p1.data = data.efetividade_p1
            sub_form.efetividade_p2.data = data.efetividade_p2
            sub_form.seguranca_p1.data = data.seguranca_p1
            sub_form.seguranca_p2.data = data.seguranca_p2
            sub_form.efetividade_obs.data = data.efetividade_obs
            sub_form.seguranca_obs.data = data.seguranca_obs
            sub_form.farmacoterapia_id.data = data.farmacoterapia_id
            sub_form.farmacoterapia_nome_remedio.data = data.farmacoterapia.nome_remedio

        form.necessidade_p1.data = avaliacao.necessidade_p1
        form.necessidade_p2.data = avaliacao.necessidade_p2
        form.adesao_p1.data = avaliacao.adesao_p1
        form.adesao_p2.data = avaliacao.adesao_p2
        form.necessidade_obs.data = avaliacao.necessidade_obs
        form.adesao_obs.data = avaliacao.adesao_obs
        form.outras_obs_p1.data = avaliacao.outras_obs_p1
        form.outras_obs_p2.data = avaliacao.outras_obs_p2
        form.outras_obs_p3.data = avaliacao.outras_obs_p3
        form.outras_obs_p4.data = avaliacao.outras_obs_p4
        form.outras_obs_p5.data = avaliacao.outras_obs_p5
        form.outras_obs_p6.data = avaliacao.outras_obs_p6
        form.outras_obs_p7.data = avaliacao.outras_obs_p7
        form.outras_obs_p8.data = avaliacao.outras_obs_p8
        form.outras_perguntas_obs.data = avaliacao.outras_perguntas_obs
        form.parecer_farmaceutico.data = avaliacao.parecer_farmaceutico

        return render_template('avaliacao_farmacoterapia.html', title='Avaliação da farmacoterapia', methods=['GET', 'POST'], form=form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/avaliacao_farmacoterapia/listar', methods=['GET', 'POST'])
@login_required
def listar_avaliacao_farmacoterapia():
    pagina = request.args.get('pagina', default=1, type=int)
    cpf = request.args.get('cpf', None, type=str)
    nome = request.args.get('nome', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return redirect(
            url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1, cpf=search_form.cpf.data, nome=search_form.nome.data))
    elif request.method == 'GET':
        search_form.cpf.data = cpf
        search_form.nome.data = nome

    qtd_pagina = 10
    query_string = []
    if cpf:
        query_string.append(Paciente.cpf == cpf)
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(Paciente.nome).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(AvaliacaoFarmacoterapia).filter(db.and_(*query_string))

    avaliacoes_farmacoterapias = query.order_by(AvaliacaoFarmacoterapia.id.desc()).paginate(pagina, qtd_pagina, error_out=False)

    return render_template('avaliacoes_farmacoterapia.html', title='Avaliações farmacoterapia',
                           avaliacoes_farmacoterapias=avaliacoes_farmacoterapias, search_form=search_form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/avaliacao_farmacoterapia/retorno/listar', methods=['GET', 'POST'])
@login_required
def listar_retorno():
    pagina = request.args.get('pagina', default=1, type=int)
    cpf = request.args.get('cpf', None, type=str)
    nome = request.args.get('nome', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return redirect(
            url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1, cpf=search_form.cpf.data, nome=search_form.nome.data))
    elif request.method == 'GET':
        search_form.cpf.data = cpf
        search_form.nome.data = nome

    qtd_pagina = 10
    query_string = []
    if cpf:
        query_string.append(Paciente.cpf == cpf)
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(Paciente.nome).ilike("%{}%".format(str_unnacented)))
    query = db.session.query(Retorno).filter(db.and_(*query_string))

    retornos = query.order_by(Retorno.id.desc()).paginate(pagina, qtd_pagina, error_out=False)

    return render_template('retornos.html', title='Retornos', retornos=retornos, search_form=search_form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/avaliacao_farmacoterapia/retorno/cadastrar/<int:id_avaliacao_farmacoterapia>', methods=['GET', 'POST'])
@login_required
def cadastrar_retorno(id_avaliacao_farmacoterapia):
    avaliacao_form = AvaliacaoProblemasFarmacoterapia()
    avaliacao_sub_form = AvaliacaoFarmacoterapiaEfetividadeSegurancaForm()

    form = RetornoForm()

    avaliacao_farmacoterapia = AvaliacaoFarmacoterapia.query.filter_by(id=id_avaliacao_farmacoterapia).first()
    retornos = Retorno.query.filter_by(avaliacao_id=id_avaliacao_farmacoterapia).all()

    if form.validate_on_submit():
        retorno = Retorno(
            observacoes=form.observacoes.data,
            compareceu=form.compareceu.data,
            data_retorno=form.data_retorno.data,
            paciente_id=avaliacao_farmacoterapia.paciente_id,
            user_id=current_user.id,
            avaliacao_farmacoterapia=avaliacao_farmacoterapia
        )
        db.session.add(retorno)
        db.session.commit()
        flash('[Retorno] Formulário respondido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_retorno', pagina=1))

    return render_template('retorno.html', title='Retorno', retornos=retornos,
                           avaliacao_farmacoterapia=avaliacao_farmacoterapia,
                           avaliacao_form=avaliacao_form,
                           form=form, avaliacao_sub_form=avaliacao_sub_form)

@blueprint.route('/estado_situacional/dispensacao_medicamentos/avaliacao_farmacoterapia/apagar/<id>', methods=['GET'])
@login_required
def apagar_avaliacao_farmacoterapia(id):
    avaliacao_farmacoterapia = AvaliacaoFarmacoterapia.query.filter_by(id=id).first()

    if avaliacao_farmacoterapia is None:
        flash('[Avaliação da farmacoterapia] Cadastro não encontrado')
        return redirect(url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1))

    if len(avaliacao_farmacoterapia.retornos) == 0:
        db.session.delete(avaliacao_farmacoterapia)
        db.session.commit()
        flash('[Avaliação da farmacoterapia] Cadastro removido com sucesso')
        return redirect(url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1))

    flash('[Avaliação da farmacoterapia] Um ou mais retornos já foram cadastrados')
    return redirect(url_for('questionario_blueprint.listar_avaliacao_farmacoterapia', pagina=1))
