def junta_respostas(origem_respostas_padrao, raw_respostas_padrao, raw_respostas_personalizadas):
    """
    Junta respostas padrão (checkbox) com respostas personalizadas. O formato de entrada
    de ambas as listas deve ser: [(key:value), (key:value)] e o retorno: [value, value].

    origem_respostas_padrao é a lista que é passada para o Forms. É a partir dele que
    consegue-se apenas os nomes.

    respostas_personalizadas: for dentro de for pra pegar apenas os values nao importando a key.
    """
    respostas_padrao = [dict(origem_respostas_padrao).get(selected) for selected in raw_respostas_padrao]
    respostas_personalizadas = [value for selected in raw_respostas_personalizadas for value in selected.values()]
    respostas = respostas_padrao + respostas_personalizadas
    respostas_tratadas = '|'.join(respostas)

    return respostas_tratadas
