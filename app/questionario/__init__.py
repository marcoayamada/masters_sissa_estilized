from flask import Blueprint

blueprint = Blueprint('questionario_blueprint', __name__, template_folder='templates', url_prefix='/questionario')
