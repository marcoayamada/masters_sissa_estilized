from flask_wtf import FlaskForm
from wtforms import SubmitField, RadioField, SelectField, TextAreaField, SelectMultipleField, StringField, FieldList, \
    BooleanField, FormField, DateField, HiddenField, Form, widgets
from wtforms.validators import NoneOf, DataRequired, Length, Optional

class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class HistoricoRemediosForm(Form):
    nomeRemedio = StringField('Nome do remédio')
    concentracao = StringField('Concentração')
    formaFarmaceutica = StringField('Forma farmacêutica')
    finalidade = StringField('Finalidade')
    comoFoiPrescrito = TextAreaField('Como foi prescrito')
    comoToma = TextAreaField('Como toma')
    quemReceitou = StringField('Quem receitou')
    produzEfeitoEsperado = BooleanField('Produz efeito esperado?')
    temDificuldadeAdquirir = BooleanField('Tem dificuldade em adquirir?')
    temDificuldadeAdministrar = BooleanField('Tem dificuldade em administrar?')
    senteReacaoAdversa = BooleanField('Sente reação adversa?')
    observacoes = StringField('Observações')

class SubForm(Form):
    outra = StringField('Outra')

def monta_form_personalizado(_comorbidades, _patologias, _estilos_vida, _alergias_medicamentos, _alergias_alimentos):

    class EstadoSituacional(FlaskForm):
        comorbidades = MultiCheckboxField('Comorbidade', choices=_comorbidades)
        patologias = RadioField('Patologia', choices=_patologias, default=_patologias[0][0])
        preocupacao = TextAreaField('Quais são suas preocupações de saúde?')
        estilos_vida = MultiCheckboxField('Estilo de vida', choices=_estilos_vida)
        alergias_medicamentos = MultiCheckboxField('Alergia a medicamentos', choices=_alergias_medicamentos)
        alergias_alimentos = MultiCheckboxField('Alergia a alimentos', choices=_alergias_alimentos)

        # CamelCase nesse caso não optativo, mas sim necessário. O js que adiciona o elemento
        # substitui '_' pelo índice. Snake_case iria quebrar a lógica.
        outrasComorbidades = FieldList(FormField(SubForm), min_entries=0, max_entries=50)
        outrasEstiloVida = FieldList(FormField(SubForm), min_entries=0, max_entries=50)
        outrasAlergiasMedicamentos = FieldList(FormField(SubForm), min_entries=0, max_entries=50)
        outrasAlergiasAlimentos = FieldList(FormField(SubForm), min_entries=0, max_entries=50)

        historicoRemediosUsados = FieldList(FormField(HistoricoRemediosForm), min_entries=0, max_entries=50)

        submit = SubmitField('Salvar')

    return EstadoSituacional()

class DispensacaoMedicamentoForm(FlaskForm):
    mensagem_check = 'Todos os medicamentos foram dispensados conforme protocolo prescrito. ' \
                     'Todas recomendações e orientações foram repassadas ao paciente de acordo ' \
                     'com o padrão do serviço de atenção farmacêutica da FAE.'

    data_retorno = DateField('Data de retorno', format='%d/%m/%Y',
                validators=[DataRequired('Preencha com uma data.')], render_kw={'autocomplete': 'off'})
    check_academico = BooleanField(mensagem_check, validators=[DataRequired('É necessário confirmar estar ciente da dispensação.')])
    remedios_dispensados = HiddenField('Remédios dispensados')
    submit = SubmitField('Salvar')

class AvaliacaoFarmacoterapiaEfetividadeSegurancaForm(FlaskForm):
    choices = [('selecione', 'Selecione'), ('Sim', 'Sim'), ('Não', 'Não'), ('Não se aplica', 'Não se aplica')]

    msg_efetividade_p1 = 'As metas terapêuticas estão sendo alcançadas para este medicamento?'
    msg_efetividade_p2 = 'A dose está adequada para atingir a meta terapêutica? (Investigar doses abaixo do recomendado)'
    msg_seguranca_p1 = 'A farmacoterapia produz novos problemas de saúde - RAM? (Investigar doses acima do preconizado)'
    msg_seguranca_p2 = 'A farmacoterapia agrava problemas de saúde pré-existentes?'

    efetividade_p1 = SelectField(msg_efetividade_p1, choices=choices,
                                 validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    efetividade_p2 = SelectField(msg_efetividade_p2, choices=choices,
                                 validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    seguranca_p1 = SelectField(msg_seguranca_p1, choices=choices,
                                 validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    seguranca_p2 = SelectField(msg_seguranca_p2, choices=choices,
                                 validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    efetividade_obs = TextAreaField('Observação', validators=[Length(max=2000, message="Digite até 2000 caracteres.")])
    seguranca_obs = TextAreaField('Observação', validators=[Length(max=2000, message="Digite até 2000 caracteres.")])

    # campos auxiliares que guardarao o nome e id da farmacoterapia (remedio)
    farmacoterapia_id = HiddenField('farmacoterapia_id')
    farmacoterapia_nome_remedio = HiddenField('farmacoterapia_nome_remedio')

class AvaliacaoProblemasFarmacoterapia(FlaskForm):
    choices = [('selecione', 'Selecione'), ('Sim', 'Sim'), ('Não', 'Não'), ('Não se aplica', 'Não se aplica')]

    msg_necessidade_p1 = 'Todas as comorbidades citadas pelo paciente estão sendo tratadas?'
    msg_necessidade_p2 = 'O paciente está tomando algum medicamento que não há um problema de saúde correspondente?'
    msg_adesao_p1 = 'O paciente compreende e é capaz de cumprir o regime posológico, concorda e adere ao tratamento numa postura ativa?'
    msg_adesao_p2 = 'Avaliar a não adesão voluntária (opta por não seguir o tratamento prescrito pelo médico) e involuntária (dificuldade de acesso, dificuldade de compreensão)'

    msg_outras_obs_p1 = 'Tem alguma forma farmacêutica, apresentação ou via de administração incorreta?'
    msg_outras_obs_p2 = 'O paciente toma algum medicamento com frequência ou horário de administração incorreto sem alteração da dose total diária?'
    msg_outras_obs_p3 = 'Tem algum medicamento com duração do tratamento incorreta?'
    msg_outras_obs_p4 = 'Tem alguma interação grave medicamento-medicamento?'
    msg_outras_obs_p5 = 'Tem alguma interação medicamento-alimento?'
    msg_outras_obs_p6 = 'Você identificou desvio de qualidade aparente nos medicamentos do paciente?'
    msg_outras_obs_p7 = 'Existe algum medicamento vencido?'
    msg_outras_obs_p8 = 'O paciente armazena os medicamentos de forma incorreta?'

    necessidade_p1 = SelectField(msg_necessidade_p1, choices=choices,
                                 validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    necessidade_p2 = SelectField(msg_necessidade_p2, choices=choices,
                                 validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    adesao_p1 = SelectField(msg_adesao_p1, choices=choices,
                            validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    adesao_p2 = SelectField(msg_adesao_p2, choices=choices,
                            validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    necessidade_obs = TextAreaField('Observação', validators=[Length(max=2000, message="Digite até 2000 caracteres.")])
    adesao_obs = TextAreaField('Observação', validators=[Length(max=2000, message="Digite até 2000 caracteres.")])

    parecer_farmaceutico = TextAreaField('Análise', validators=[Length(max=10000, message="Digite até 10000 caracteres.")])

    efetividades_segurancas = FieldList(FormField(AvaliacaoFarmacoterapiaEfetividadeSegurancaForm), min_entries=0)

    #outras perguntas
    outras_obs_p1 = SelectField(msg_outras_obs_p1, choices=choices,
                            validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p2 = SelectField(msg_outras_obs_p2, choices=choices,
                            validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p3 = SelectField(msg_outras_obs_p3, choices=choices,
                                validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p4 = SelectField(msg_outras_obs_p4, choices=choices,
                                validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p5 = SelectField(msg_outras_obs_p5, choices=choices,
                                validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p6 = SelectField(msg_outras_obs_p6, choices=choices,
                                validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p7 = SelectField(msg_outras_obs_p7, choices=choices,
                                validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_obs_p8 = SelectField(msg_outras_obs_p8, choices=choices,
                                validators=[NoneOf('selecione', message="Escolha uma opção válida")])
    outras_perguntas_obs = TextAreaField('Observação', validators=[Length(max=2000, message="Digite até 2000 caracteres.")])

    submit = SubmitField('Salvar')

class RetornoForm(FlaskForm):
    observacoes = TextAreaField('Observações', validators=[Length(max=2000, message="Digite até 2000 caracteres.")])
    data_retorno = DateField('Próximo retorno', format='%d/%m/%Y',
                validators=[Optional()], render_kw={'autocomplete': 'off'})
    compareceu = BooleanField('Compareceu à consulta de retorno?')

    submit = SubmitField('Salvar')
