from flask import Blueprint

blueprint = Blueprint('admin_blueprint', __name__, template_folder='templates', url_prefix='/admin')
