from flask import redirect, url_for, render_template, flash, request, jsonify
from app.admin import blueprint
from app import db
from flask_login import current_user
from app.base.models import User, Unaccent, Search
from flask_login import login_required
from app.admin.forms import CreateAccountForm, EditAccountForm
from app.base.forms import SearchPessoaForm
import random
import datetime


@blueprint.route('/users/listar', methods=['GET', 'POST'])
@login_required
def users():
    pagina = request.args.get('pagina', default=1, type=int)
    username = request.args.get('username', None, type=str)
    nome = request.args.get('nome', None, type=str)
    email = request.args.get('email', None, type=str)

    search_form = SearchPessoaForm()
    if search_form.validate_on_submit():
        return redirect(url_for('admin_blueprint.users', pagina=1,
                                username=search_form.username.data,
                                nome=search_form.nome.data,
                                email=search_form.email.data,
                                ))
    elif request.method == 'GET':
        search_form.username.data = username
        search_form.nome.data = nome
        search_form.email.data = email

    qtd_pagina = 10
    query_string = []
    if username:
        str_unnacented = Search.removeAccent(username)
        query_string.append(Unaccent(User.username).ilike("%{}%".format(str_unnacented)))
    if nome:
        str_unnacented = Search.removeAccent(nome)
        query_string.append(Unaccent(User.name).ilike("%{}%".format(str_unnacented)))
    if email:
        str_unnacented = Search.removeAccent(email)
        query_string.append(Unaccent(User.email).ilike("%{}%".format(str_unnacented)))

    query = db.session.query(User).filter(db.and_(*query_string))

    if current_user.is_authenticated and current_user.admin:
        form = CreateAccountForm()
        users = query.order_by("username").paginate(pagina, qtd_pagina, error_out=False)

        return render_template('users.html', title='Usuários', users=users, form=form, search_form=search_form)
    return redirect(url_for('base_blueprint.login'))

@blueprint.route('/add_user', methods=['GET', 'POST'])
@login_required
def add_user():
    form = CreateAccountForm()
    if form.validate_on_submit():
        user = User(
         username=form.username.data,
         name=form.name.data,
         email=form.email.data,
         active=True,
         password=str(random.randint(1, 99999))
        )
        db.session.add(user)
        db.session.commit()
        flash('Usuário adicionado com sucesso')
        return jsonify(status='ok')
    return render_template('add_user.html', methods=['GET', 'POST'], form=form)

@blueprint.route('/edit_user/<id>', methods=['GET', 'POST'])
@login_required
def edit_user(id):
    user = User.query.filter_by(id=id).first()
    form = EditAccountForm(user.email)
    if form.validate_on_submit():
        user.name = form.name.data
        user.email = form.email.data
        user.active = form.active.data
        user.updated_in = datetime.datetime.utcnow()
        db.session.commit()
        flash('Usuário editado com sucesso')
        return jsonify(status='ok')
    elif request.method == 'GET':
        form.username.data = user.username
        form.name.data = user.name
        form.email.data = user.email
        form.active.data = user.active
    return render_template('edit_user.html', methods=['GET', 'POST'], form=form)

