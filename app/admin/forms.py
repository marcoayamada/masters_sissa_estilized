from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, StringField, SubmitField, BooleanField
from wtforms.validators import InputRequired, Email, DataRequired, ValidationError, EqualTo
from app.base.models import User



class CreateAccountForm(FlaskForm):
    username = StringField('Username'     , id='username_create' , validators=[DataRequired()])
    name = StringField('Nome'     , id='name_create' , validators=[DataRequired()])
    email    = StringField('Email'        , id='email_create'    , validators=[DataRequired(), Email()])
    #password = PasswordField('Password' , id='pwd_create'      , validators=[DataRequired()])
    submit = SubmitField('Salvar')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Esse email já está cadastrado. Informe outro.')

    def validate_username(self, username):
        user = User.query.filter_by(email=username.data).first()
        if user is not None:
            raise ValidationError('Esse usuário já está cadastrado. Tente outro.')

class EditAccountForm(FlaskForm):
    #funcao para editar
    def __init__(self, original_email, *args, **kwargs):
        super(EditAccountForm, self).__init__(*args, **kwargs)
        self.original_email = original_email

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).all()
        if len(user) > 1:
            raise ValidationError('Esse email já está cadastrado. Informe outro.')

    username = StringField('Username', id='username_create', validators=[DataRequired()], render_kw={'readonly': True})
    name = StringField('Nome', id='name_create', validators=[DataRequired()])
    email = StringField('Email', id='email_create', validators=[DataRequired(), Email()])
    active = BooleanField('Ativo')
    submit = SubmitField('Salvar')

# reset and forget password
class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email(message="Email inválido.")])
    submit = SubmitField('Enviar link')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('Email não encontrado.')

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Senha', validators=[DataRequired()])
    password2 = PasswordField('Confirmar senha', validators=[DataRequired(), EqualTo('password', message="A senha deve ser igual.")])
    submit = SubmitField('Resetar')
